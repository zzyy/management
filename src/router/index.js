import Vue from 'vue'
import Router from 'vue-router'
// import {Login, Err404} from 'pages'
import {routers, setMenuTree} from './router'

Vue.use(Router)

export const router = new Router({
  routes: routers.router
})

export let getMenuFuc = function (list) {
  setMenuTree(list)
}

if (localStorage.getItem('token')) {
  getMenuFuc(JSON.parse(localStorage.getItem('menuTree')))
}

router.beforeEach((to, from, next) => {
  if (!localStorage.getItem('token') && to.name !== 'login') {
    next('/login')
  } else {
    next()
  }
})
