import Vue from 'vue'
import {router} from './index'
import {
  Login,
  ComponentManage,
  DeviceManage,
  BindComponent,
  StateMonitor,
  UserManage,
  RoleManage,
  OriginManage,
  RangeManage,
  QuickAuth} from 'pages'

export let initMenu = [
  {
    path: '', redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/device/bindcomponent/:id',
    name: 'bindcomponent',
    component: BindComponent
  }
]
export let menu = {
  'components': {
    path: '/components',
    name: 'components',
    component: ComponentManage
  },
  'device': {
    path: '/device',
    name: 'device',
    component: DeviceManage
  },
  'state': {
    path: '/state',
    name: 'state',
    component: StateMonitor
  },
  'origin': {
    path: '/origin',
    name: 'origin',
    component: OriginManage
  },
  'range': {
    path: '/range',
    name: 'range',
    component: RangeManage
  },
  'user': {
    path: '/user',
    name: 'user',
    component: UserManage
  },
  'role': {
    path: '/role',
    name: 'role',
    component: RoleManage
  },
  'quick': {
    path: '/quick',
    name: 'quick',
    component: QuickAuth
  }
}

export let menuList = []
export const setMenuTree = function (menuTree) {
  let temp = new Vue({router})
  // menuList = []
  if (menuTree !== null) {
    handleFor(menuTree)
    temp.$router.addRoutes(menuList)
    temp.$router.addRoutes([{path: '*', redirect: '/' + menuList[0].name}])
  }
}

const handleFor = function (data) {
  for (let i = 0; i < data.length; i++) {
    if (typeof menu[data[i].url] !== 'undefined') {
      menuList.push(menu[data[i].url])
    }
    if (data[i].list) {
      handleFor(data[i].list)
    }
  }
}

export const routers = {
  router: initMenu
}
