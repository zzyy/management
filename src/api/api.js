const apiModule = {
  /* 权限管理&&系统管理 */
  // 用户认证
  login: {
    url: '/api/sys/auth/login',
    method: 'post'
  },
  logout: {
    url: '/api/sys/auth/logout',
    method: 'get'
  },
  /* 部门管理 */
  // 选择部门
  selectDepartment: {
    url: '/api/sys/dept/select',
    method: 'get'
  },
  /* 功能管理 */
  getControlMenu: {
    url: '/api/sys/right/menu',
    method: 'get'
  },

  /* 角色管理 */
  // 设置用户的角色
  addRole: {
    url: '/api/sys/role/add',
    method: 'post'
  },
  // 删除用户的角色
  deleteRole: {
    url: '/api/sys/role/delete/',
    method: 'delete'
  },
  // 获取角色列表
  getRoleList: {
    url: '/api/sys/role/pageList',
    method: 'get'
  },
  // 查角色下拉框
  queryRole: {
    url: '/api/sys/role/select',
    method: 'get'
  },
  // 更新用户的角色
  updateRole: {
    url: '/api/sys/role/update',
    method: 'post'
  },
  // 用户详细信息
  detailRole: {
    url: '/api/sys/role/info/',
    method: 'get'
  },
  /* 用户管理 */
  // 添加用户
  addUser: {
    url: '/api/sys/user/add',
    method: 'post'
  },
  // 修改当前用户的密码
  updatePassword: {
    url: '/api/sys/user/changePwd',
    method: 'put'
  },
  // 删除用户
  deleteUser: {
    url: '/api/sys/user/delete/',
    method: 'delete'
  },
  // 冻结用户
  freezeUser: {
    url: '/api/sys/user/freeze/',
    method: 'get'
  },
  // 用户分页查询
  userList: {
    url: '/api/sys/user/pageList',
    method: 'get'
  },
  // 解除冻结用户
  unfreezeUser: {
    url: '/api/sys/user/unfreeze/',
    method: 'get'
  },
  // 修改管理员
  updateUser: {
    url: '/api/sys/user/update',
    method: 'post'
  },
  // 详细信息
  detailsUser: {
    url: '/api/sys/user/info/',
    method: 'get'
  },
  /* 部件管理 */
  // 部件列表查询
  componentList: {
    url: '/api/fittings/list',
    method: 'post'
  },
  // 部件新增
  addComponent: {
    url: '/api/fittings/add',
    method: 'post'
  },
  // 部件类型下拉
  componentTypeList: {
    url: '/api/fittings/fittingsType',
    method: 'get'
  },
  // 根据id查询单条
  componentDetail: {
    url: '/api/fittings/',
    method: 'get'
  },
  // 部件修改
  modifyComponent: {
    url: '/api/fittings/update',
    method: 'post'
  },
  /* 设备管理 */
  // 组织机构（包含虚拟机构）
  departmentTree: {
    url: '/api/sys/faceTerminal/department',
    method: 'get'
  },
  // isEnable传值修改部件启用/停用,isEnable不传值删除部件,两个接口合在一起
  deleteComponent: {
    url: '/api/terminalFittings/updateFittings',
    method: 'post'
  },
  // 新增设备部件
  addDeviceComponent: {
    url: '/api/terminalFittings/addTermFittings',
    method: 'post'
  },
  // 设备列表查询
  deviceList: {
    url: '/api/sys/faceTerminal/terminalList',
    method: 'post'
  },
  // 设备类型下拉
  deviceTypeList: {
    url: '/api/sys/faceTerminal/faceTerminalType',
    method: 'get'
  },
  // 设备类型下拉(新增修改)
  deviceTypeListAddOrMod: {
    url: '/api/sys/faceTerminal/terminalType',
    method: 'get'
  },
  // 场景类型下拉
  placeList: {
    url: '/api/sys/faceTerminal/pfPlace',
    method: 'get'
  },
  // 设备 - 所属区域树结构
  deviceOfAreaTree: {
    url: '/api/sys/faceTerminal/purview',
    method: 'get'
  },
  // 根据id查询设备
  deviceDetail: {
    url: '/api/sys/faceTerminal/',
    method: 'get'
  },
  // 查询设备下所有部件
  componentOfDevice: {
    url: '/api/terminalFittings/all/',
    method: 'get'
  },
  // 新增设备
  addDevice: {
    url: '/api/sys/faceTerminal/add',
    method: 'post'
  },
  // 修改设备
  updateDevice: {
    url: '/api/sys/faceTerminal/update',
    method: 'post'
  },
  // 删除设备
  deleteDevice: {
    url: '/api/sys/faceTerminal/delete',
    method: 'get'
  },
  /* 状态监控 */
  // 根据区域查询组织下的设备
  orgDeviceOfArea: {
    url: '/api/sys/faceTerminal/purview/',
    method: 'get'
  },
  // 分组查询设备下的部件
  componentOfDeviceForGroup: {
    url: '/api/terminalFittings/',
    method: 'get'
  },
  /* 比对源管理 */
  // 查询比对源结构🌲
  originTree: {
    url: '/api/sys/faceTerminal/findPfRegion',
    method: 'get'
  },
  // 比对源列表查询
  regionList: {
    url: '/api/sys/region/regionList',
    method: 'post'
  },
  // 删除比对源
  deleteOrigin: {
    url: '/api/sys/region/delete',
    method: 'get'
  },
  // 新增
  addOrigin: {
    url: '/api/sys/region/add',
    method: 'post'
  },
  // 详情
  detailsOrigin: {
    url: '/api/sys/region', // /{regionId}
    method: 'get'
  },
  // 修改
  updateOrigin: {
    url: '/api/sys/region/update',
    method: 'post'
  },
  // 菜单管理
  // 菜单树
  getMenuTree: {
    url: '/api/sys/menu/list',
    method: 'get'
  },

  // 范围管理
  rangeList: {
    url: '/api/sys/inventedDepartment/pageList',
    method: 'get'
  },
  // 新增修改范围
  updateRange: {
    url: '/api/sys/inventedDepartment/save',
    method: 'post'
  },
  // 删除范围
  deleteRange: {
    url: '/api/sys/inventedDepartment/delete/',
    method: 'delete'
  },
  // 快速授权
  quickAuth: {
    url: '/api/sys/role/fast',
    method: 'post'
  },
  // 菜单权限
  menuAuth: {
    url: '/api/sys/menu/nav',
    method: 'get'
  }
}

export default apiModule
