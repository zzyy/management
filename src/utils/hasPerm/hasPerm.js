export default function hasPermission (permission) {
  let myBtns = JSON.parse(localStorage.getItem('permissions'))
  return myBtns.indexOf(permission) > -1
}
