import axios from 'axios'
import { router } from '../../router/index'
import {Message} from 'element-ui'

const root = process.env.API_URL

// 请求时
axios.interceptors.request.use(
  config => {
    let token = localStorage.getItem('token')
    if (token !== null) {
      config.headers.Authorization = token
    } else {
      router.push({name: 'login'})
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 请求完成
axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.resolve(error.response)
  }
)

function successState (res) {
  // 判断后台返回错误码
  if (res.data.code === 405) {
    Message.warning(res.data.message + '，请重新登录')
    console.log(res.data.code, res.data.message)
    router.push({name: 'login'})
  }
  if (res.data.code === 406) {
    Message(res.data.message)
    router.push({name: 'login'})
  }
  if (res.data.code === 600) {
    Message.warning(res.data.message)
  }
}

function errorState (err) {
  // 状态码为200，直接返回数据
  if (err && (err.status === 200 || err.status === 304 || err.status === 400)) {
    return err
  } else {
    console.log('网络异常')
  }
}

const httpServer = (opts, data) => {
  let Public = {}
  let httpDefaultOpts = {
    method: opts.method,
    url: opts.url,
    timeout: 5000,
    baseURL: root,
    // get参数
    params: Object.assign(Public, data),
    data: Object.assign(Public, data),
    headers: opts.method === 'get' ? {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    } : {
      'X-Requested-with': 'XMLHttpRequest',
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (opts.method === 'get') {
    delete httpDefaultOpts.data
  } else {
    delete httpDefaultOpts.params
  }

  return new Promise((resolve, reject) => {
    axios(httpDefaultOpts)
      .then(res => {
        successState(res)
        resolve(res.data)
      })
      .catch(err => {
        errorState(err)
        reject(err)
      })
  })
}

export default httpServer
