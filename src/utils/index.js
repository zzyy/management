import ajax from './axios'
import hasPerm from './hasPerm'
export {
  ajax,
  hasPerm
}
