import menuParent from './images/icon_menu_parent.png'
import menuComponent from './images/icon_menu_component.png'
import menuDevice from './images/icon_menu_device.png'
import menuState from './images/icon_menu_state.png'
import menuUser from './images/icon_menu_user.png'
import menuRole from './images/icon_menu_role.png'
import menuQuick from './images/icon_menu_quick.png'
import menuOrigin from './images/icon_menu_comp_origion.png'
import menuRange from './images/icon_menu_range.png'

export {
  menuParent,
  menuComponent,
  menuDevice,
  menuState,
  menuUser,
  menuRole,
  menuQuick,
  menuOrigin,
  menuRange
}

export default {
  'deviceManage': {
    name: '设备管理',
    page: 'deviceManage',
    image: menuParent
  },
  'components': {
    name: '部件管理',
    page: 'components',
    image: menuComponent
  },
  'device': {
    name: '设备管理',
    page: 'device',
    image: menuDevice
  },
  'state': {
    name: '状态监控',
    page: 'state',
    image: menuState
  },
  'bgManage': {
    name: '后台管理',
    page: 'bgManage',
    image: menuParent
  },
  'origin': {
    name: '比对源管理',
    page: 'origin',
    image: menuOrigin
  },
  'range': {
    name: '范围管理',
    page: 'range',
    image: menuRange
  },
  'user': {
    name: '用户管理',
    page: 'user',
    image: menuUser
  },
  'role': {
    name: '角色管理',
    page: 'role',
    image: menuRole
  },
  'quick': {
    name: '快速授权',
    page: 'quick',
    image: menuQuick
  }
}

/*
export default [
  {
    name: '设备管理',
    page: 'deviceManage',
    image: menuParent,
    child: [
      {
        name: '部件管理',
        page: 'components',
        image: menuComponent
      },
      {
        name: '设备管理',
        page: 'device',
        image: menuDevice
      },
      {
        name: '状态监控',
        page: 'state',
        image: menuState
      }
    ]
  },
  {
    name: '后台管理',
    page: 'bgManage',
    image: menuParent,
    child: [
      {
        name: '比对源管理',
        page: 'origin',
        image: menuOrigin
      },
      {
        name: '范围管理',
        page: 'range',
        image: menuRange
      },
      {
        name: '用户管理',
        page: 'user',
        image: menuUser
      },
      {
        name: '角色管理',
        page: 'role',
        image: menuRole
      },
      {
        name: '快速授权',
        page: 'quick',
        image: menuQuick
      }
    ]
  }
]
*/
