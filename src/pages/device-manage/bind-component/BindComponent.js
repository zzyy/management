import vMessageBox from 'components/message-box'
export default {
  data () {
    return {
      deviceData: {},
      componentData: [],
      addComponentData: [],
      listComponent: {},
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0,
        content: ''
      },
      isAbleData: {
        id: '',
        state: 0,
        type: ''
      },
      componentReq: {
        size: 15,
        current: 1,
        fittingsName: '',
        fittingsType: '',
        fittingsModel: '',
        fittingsManufacturer: ''
      },
      componentTypes: [],
      loading1: true,
      loading2: true
    }
  },
  components: {
    vMessageBox
  },
  mounted () {
    let id = this.$route.params.id
    this._getDeviceDetails(id)
    this._getComponentData(id)
    this._getComponentType()
    this._getComponentList()
  },
  methods: {
    _getDeviceDetails (id) {
      // device details
      let deviceDetail = {
        url: this.$api.deviceDetail.url + id,
        method: this.$api.deviceDetail.method
      }
      this.$ajax(deviceDetail).then(
        res => {
          if (res.code === 200) {
            let types = JSON.parse(localStorage.getItem('deviceTypes'))
            res.content.reportModeName = ''
            res.content.deviceType = ''
            // 翻译设备类型
            types.forEach(item => {
              if (item.typeCode === res.content.faceTerminalType) {
                res.content.deviceType = item.typeName
              }
            })
            // 翻译上报模式
            if (res.content.reportMode === '0') {
              res.content.reportModeName = '主动上报'
            } else if (res.content.reportMode === '1') {
              res.content.reportModeName = '被动上报'
            }
            this.deviceData = res.content
          }
        }
      )
    },
    _getComponentData (id) {
      // component details
      let componentOfDevice = {
        url: this.$api.componentOfDevice.url + id,
        method: this.$api.componentOfDevice.method
      }
      this.$ajax(componentOfDevice).then(
        res => {
          if (res.code === 200) {
            this.loading1 = false
            this.componentData = res.content
          }
        }
      )
    },
    changeEnable (id, isEnable, type) {
      // open component Box
      let enable = 0
      let enableText = ''
      if (isEnable === '启用') {
        enable = 0
        enableText = '停用'
      } else {
        enable = 1
        enableText = '启用'
      }
      this.box = {
        show: true,
        title: '操作',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 300,
        content: `您确定${enableText}吗？`
      }
      this.isAbleData = {
        id: id,
        state: enable,
        type: type
      }
    },
    _changeEnableHandle () {
      // update component
      let params = {
        fittingsId: this.isAbleData.id,
        isEnable: this.isAbleData.state
      }
      this.$ajax(this.$api.deleteComponent, params).then(
        res => {
          if (res.code === 200) {
            this.closeBox()
            this._getComponentData(this.isAbleData.id)
            this.$message({
              message: res.message,
              type: 'success'
            })
          } else {
            this.$message({
              message: res.message,
              type: 'warning'
            })
          }
        }
      )
    },
    deleteComponent (id, type) {
      this.box = {
        show: true,
        title: '操作',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 300,
        content: `您确定删除该条信息吗？`
      }
      this.isAbleData = {
        id: id,
        state: null,
        type: type
      }
    },
    _deleteComponentHandle () {
      // delete component
      let params = {
        fittingsId: this.isAbleData.id,
        isEnable: ''
      }
      this.$ajax(this.$api.deleteComponent, params).then(
        res => {
          if (res.code === 200) {
            this.closeBox()
            this._getComponentData(this.isAbleData.id)
            this.$message({
              message: res.message,
              type: 'success'
            })
          } else {
            this.$message({
              message: res.message,
              type: 'warning'
            })
          }
        }
      )
    },
    submitHandle () {
      // it's component info box callback
      if (this.isAbleData.type === 'enable') {
        this._changeEnableHandle()
      } else if (this.isAbleData.type === 'delete') {
        this._deleteComponentHandle()
      }
    },
    closeBox (val) {
      // it's component info box callback
      this.box.show = val
    },
    // all component list part
    // component type drop list
    _getComponentType () {
      this.$ajax(this.$api.componentTypeList).then(
        res => {
          if (res.code === 200) {
            this.componentTypes = res.content
          } else {
            console.error('获取部件类型失败！')
          }
        }
      )
    },
    _getComponentList () {
      this.$ajax(this.$api.componentList, this.componentReq).then(
        res => {
          if (res.code === 200) {
            this.loading2 = false
            this.loading = false
            this.listComponent = res.content
          } else {
            console.error('获取部件列表失败！')
          }
        }
      )
    },
    // add component,可以重复添加
    addComponent (item) {
      this.addComponentData.unshift(item)
      // let index = this.addComponentData.indexOf(item)
      // if (index === -1) {
      //   this.addComponentData.unshift(item)
      // } else {
      //   this.$message({
      //     message: '这条已经添加过了～',
      //     type: 'warning'
      //   })
      // }
    },
    removeComponent (index, rows) {
      rows.splice(index, 1)
      // this.addComponentData.forEach(item => {
      //   if (item.id === id) {
      //     let index = this.addComponentData.indexOf(item)
      //     if (index > -1) {
      //       this.addComponentData.splice(index, 1)
      //     }
      //   }
      // })
    },
    saveComponent () {
      let id = this.$route.params.id
      let params = {
        terminalId: id,
        fittingsList: []
      }
      this.addComponentData.forEach(item => {
        let fittingObj = {
          fittingsId: item.id,
          fittingsType: item.fittingsType
        }
        params.fittingsList.push(fittingObj)
      })
      this.$ajax(this.$api.addDeviceComponent, params).then(
        res => {
          if (res.code === 200) {
            this._getComponentData(id)
            this.$router.push({name: 'device'})
            this.$message({
              message: res.message,
              type: 'success'
            })
          } else {
            this.$message({
              message: res.message,
              type: 'error'
            })
          }
        }
      )
    },
    back () {
      this.$router.go(-1)
    },
    handleCurrentChange (val) {
      this.componentReq.current = val
      this._getComponentList()
    }
  }
}
