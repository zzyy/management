import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
export default {
  data () {
    return {
      deviceReq: {
        size: 12,
        current: 1,
        faceTerminalName: '',
        faceTerminalType: '',
        faceTerminalIp: '',
        deptId: '',
        regionIds: []
      },
      treeData: [],
      defaultProps: {
        children: 'deptTree',
        label: 'name'
      },
      filterText: '',
      originData: [],
      originTreeData: [],
      originProps: {
        value: 'id',
        label: 'name',
        children: 'region'
      },
      deviceTypes: [],
      deviceTypesAddOrMod: [],
      placeTypes: [],
      deviceData: [],
      detailsData: {},
      detailsOfComponent: [],
      loading: true,
      loadingDetails: true,
      breadcrumbList: [
        {name: '设备管理', to: ''},
        {name: '设备列表', to: ''}
      ],
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      addBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delId: '',
      statusSelect: [{value: '0', label: '异常'}, {value: '1', label: '在线'}, {value: '2', label: '离线'}],
      addFormData: {
        terminalId: '',
        faceTerminalName: '', // 设备名称
        faceTerminalPassword: '', // 登录密码
        faceTerminalUser: '', // 登录账户
        faceTerminalStatus: '', // 设备状态
        faceTerminalType: '', // 设备类型
        faceTerminalDriverId: '', // 设备驱动
        faceTerminalChannel: '', // 设备通道号
        faceTerminalIp: '', // IP地址
        faceTerminalMac: '', // mac地址
        streamMode: '', // 码流模式
        faceTerminalPort: '', // 端口
        regTime: '', // 上线时间
        reportMode: '', // 上报模式
        authorizationExamineTime: '', // 有效期
        placeId: '', // 场景类型
        regionIds: [] // 比对源
      },
      rules: {
        faceTerminalName: [
          {required: true, message: '请输入设备名称', trigger: 'blur'},
          {min: 1, max: 50, message: '长度在1到50个字符', trigger: 'blur'},
          {pattern: /^[a-z\d\u4e00-\u9fa5]+$/i, message: '设备名称不能包含特殊字符'}
        ],
        faceTerminalPassword: [
          {required: true, message: '请输入登录密码', trigger: 'blur'},
          {pattern: /^[a-zA-Z0-9!@#$%^&*?]{1,20}$/, message: '请输入正确密码（1到20位，字母，数字，特殊符号）', trigger: 'blur'}
        ],
        faceTerminalUser: [
          {required: true, message: '请输入登录账户', trigger: 'blur'},
          {pattern: /^[a-zA-Z0-9_-]{2,20}$/, message: '请输入正确账户（2到20位，字母，数字，下划线，减号）', trigger: 'blur'}
        ],
        faceTerminalStatus: [
          {required: true, message: '请选择设备状态', trigger: 'change'}
        ],
        faceTerminalType: [
          {required: true, message: '请选择设备类型', trigger: 'change'}
        ],
        faceTerminalDriverId: [
          {required: true, message: '请选择设备驱动', trigger: 'change'}
        ],
        faceTerminalChannel: [
          {required: true, message: '请输入设备通道号', trigger: 'blur'},
          {pattern: /^[0-9]{1,100}$/, message: '设备通道号必须为数字'}
        ],
        faceTerminalIp: [
          {required: true, message: '请输入IP地址', trigger: 'blur'},
          {
            pattern: /^(?:(?:2[0-4][0-9]\.)|(?:25[0-5]\.)|(?:1[0-9][0-9]\.)|(?:[1-9][0-9]\.)|(?:[0-9]\.)){3}(?:(?:2[0-5][0-5])|(?:25[0-5])|(?:1[0-9][0-9])|(?:[1-9][0-9])|(?:[0-9]))$/,
            message: '请输入正确的IP地址'
          }
        ],
        faceTerminalMac: [
          {required: true, message: '请输入mac地址', trigger: 'blur'},
          {pattern: /^[A-F0-9]{2}(-[A-F0-9]{2}){5}$/, message: 'mac地址格式或者大小写错误'}
        ],
        streamMode: [
          {required: true, message: '请选择码流模式', trigger: 'change'}
        ],
        faceTerminalPort: [
          {required: true, message: '请输入端口', trigger: 'blur'},
          {pattern: /^[0-9]{1,6}$/, message: '请输入正确的端口'}
        ],
        regTime: [
          {required: true, message: '请选择上线时间', trigger: 'change'}
        ],
        reportMode: [
          {required: true, message: '请选择上报模式', trigger: 'change'}
        ],
        placeId: [
          {required: true, message: '请选择场景类型', trigger: 'change'}
        ]
      }
    }
  },
  mounted () {
    this._getTreeData()
    this._getOriginSelectData()
    this._getDeviceTypesType()
    this._getDeviceList()
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox
  },
  watch: {
    filterText (val) {
      this.$refs.myTree.filter(val)
    }
  },
  methods: {
    _getTreeData () {
      this.$ajax(this.$api.departmentTree).then(
        res => {
          if (res.code === 200) {
            // 由于后台传过来的是一个对象结构，所以需要push进一个数组
            this.treeData = res.content
          }
        }
      )
    },
    handleNodeClick (data) {
      this.deviceReq.current = 1
      this.deviceReq.deptId = data.id
      this._getDeviceList()
    },
    filterNode (value, data) {
      if (!value) return true
      return data.name.indexOf(value) !== -1
    },
    _getOriginSelectData () {
      this.$ajax(this.$api.originTree).then(
        res => {
          if (res.code === 200) {
            // 由于后台传过来的是一个对象结构，所以需要push进一个数组
            res.content.region.map(item => {
              if (item.region.length === 0) {
                item.region = null
              }
            })
            this.originData.push(res.content)
          }
        }
      )
    },
    changeOrigin (val) {
      let id = val[val.length - 1]
      this.deviceReq.regionIds = []
      if (typeof id !== 'undefined') {
        this.deviceReq.regionIds.push(id)
      }
    },
    _getDeviceTypesType () {
      // 获取设备类型下拉
      this.$ajax(this.$api.deviceTypeList).then(
        res => {
          if (res.code === 200) {
            this.deviceTypes = res.content
            localStorage.setItem('deviceTypes', JSON.stringify(res.content))
          }
        }
      )
      // 获取设备类型下拉 新增 或 修改
      this.$ajax(this.$api.deviceTypeListAddOrMod).then(
        res => {
          if (res.code === 200) {
            this.deviceTypesAddOrMod = res.content
          }
        }
      )
      // 场景类型下拉
      this.$ajax(this.$api.placeList).then(
        res => {
          if (res.code === 200) {
            this.placeTypes = res.content
          }
        }
      )
    },
    _getDeviceList () {
      this.$ajax(this.$api.deviceList, this.deviceReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.deviceData = res.content
          } else {
            console.error('获取设备列表失败！')
          }
        }
      )
    },
    _getDetails (id, isMod) {
      // left details
      let deviceDetail = {
        url: this.$api.deviceDetail.url + id,
        method: this.$api.deviceDetail.method
      }
      this.$ajax(deviceDetail).then(
        res => {
          if (res.code === 200) {
            // 设备类型
            let terminalType = res.content.faceTerminalType
            this.deviceTypesAddOrMod.forEach(item => {
              if (item.typeCode === terminalType) {
                res.content.showTerminalType = item.typeName
              }
            })
            // 场景类型
            let placeType = res.content.placeId
            this.placeTypes.forEach(item => {
              if (item.id === placeType) {
                res.content.placeType = item.name
              }
            })
            this.detailsData = res.content
            // 如果是修改
            if (isMod) {
              // this.isModify = isMod
              if (res.content.regionIds === '') {
                res.content.regionIds = []
              }
              this.addFormData = res.content
              this.$refs.origin.setCheckedKeys(res.content.regionIds)
            } else {
              this.addFormData = {
                terminalId: '',
                faceTerminalName: '', // 设备名称
                faceTerminalPassword: '', // 登录密码
                faceTerminalUser: '', // 登录账户
                faceTerminalStatus: '', // 设备状态
                faceTerminalType: '', // 设备类型
                faceTerminalDriverId: '', // 设备驱动
                faceTerminalChannel: '', // 设备通道号
                faceTerminalIp: '', // IP地址
                faceTerminalMac: '', // mac地址
                streamMode: '', // 码流模式
                faceTerminalPort: '', // 端口
                regTime: '', // 上线时间
                reportMode: '', // 上报模式
                authorizationExamineTime: '', // 有效期
                placeId: '', // 场景类型
                regionIds: [] // 比对源
              }
            }
          }
        }
      )
    },
    _getDetailsComponent (id, isMod) {
      // right details
      let componentOfDevice = {
        url: this.$api.componentOfDevice.url + id,
        method: this.$api.componentOfDevice.method
      }
      this.$ajax(componentOfDevice).then(
        res => {
          if (res.code === 200) {
            this.loadingDetails = false
            this.detailsOfComponent = res.content
          }
        }
      )
    },
    detailsHandle (id) {
      this.box = {
        show: true,
        title: '详细',
        submitText: '确定',
        cancleText: null,
        width: 1060,
        height: 640
      }
      // clear detailsData
      this.detailsData = {}
      this._getDetails(id, false)
      this._getDetailsComponent(id, false)
    },
    submitHandle () {
      this.closeDetailBox()
    },
    closeDetailBox (val) {
      this.box.show = val
    },
    // 打开新增弹出层
    addHandle () {
      this.addBox = {
        show: true,
        title: '新增',
        submitText: '确定',
        cancleText: '取消',
        width: 1240,
        height: 800
      }
      this.isModify = false
      // 请求比对源🌲数据
      this.$ajax(this.$api.originTree).then(
        res => {
          if (res.code === 200) {
            this.originTreeData = []
            // 由于后台传过来的是一个对象结构，所以需要push进一个数组
            this.originTreeData.push(res.content)
          }
        }
      )
    },
    // 提交新增请求
    submitAddHandle () {
      if (this.addFormData.regionIds.length === 0) {
        this.$message({
          type: 'warning',
          message: '请选择对比源'
        })
        return
      }
      this.$refs.addForm.validate((valid) => {
        if (valid) {
          this.addFormData.regTime = this.addFormData.regTime + '000000'
          this.addFormData.authorizationExamineTime = this.addFormData.authorizationExamineTime + '235959'
          if (!this.isModify) {
            // 新增
            this.$ajax(this.$api.addDevice, this.addFormData).then(
              res => {
                if (res.code === 200) {
                  this.$message({
                    type: 'success',
                    message: res.message
                  })
                  this._getDeviceList()
                  this.closeAddBox(false)
                } else {
                  this.$message({
                    type: 'warning',
                    message: res.message
                  })
                }
              }
            )
          } else {
            // 修改
            if (this.addFormData.regTime.length > 14) {
              this.addFormData.regTime = this.addFormData.regTime.replace(/-/g, '').replace(/:/g, '').replace(/ /g, '').substring(0, 8) + '000000'
            }
            if (this.addFormData.authorizationExamineTime.length > 14) {
              this.addFormData.authorizationExamineTime = this.addFormData.authorizationExamineTime.replace(/-/g, '').replace(/:/g, '').replace(/ /g, '').substring(0, 8) + '235959'
            }
            this.$ajax(this.$api.updateDevice, this.addFormData).then(
              res => {
                if (res.code === 200) {
                  this.$message({
                    type: 'success',
                    message: res.message
                  })
                  this._getDeviceList()
                  this.closeAddBox(false)
                } else {
                  this.$message({
                    type: 'warning',
                    message: res.message
                  })
                }
              }
            )
          }
        } else {
          return false
        }
      })
    },
    closeAddBox (val) {
      this.addBox.show = val

      this.$refs.addForm.resetFields()

      this.addFormData = {
        terminalId: '',
        faceTerminalName: '', // 设备名称
        faceTerminalPassword: '', // 登录密码
        faceTerminalUser: '', // 登录账户
        faceTerminalStatus: '', // 设备状态
        faceTerminalType: '', // 设备类型
        faceTerminalDriverId: '', // 设备驱动
        faceTerminalChannel: '', // 设备通道号
        faceTerminalIp: '', // IP地址
        faceTerminalMac: '', // mac地址
        streamMode: '', // 码流模式
        faceTerminalPort: '', // 端口
        regTime: '', // 上线时间
        reportMode: '', // 上报模式
        authorizationExamineTime: '', // 有效期
        placeId: '', // 场景类型
        regionIds: [] // 比对源
      }
    },
    modifyHandle (id) {
      this.addFormData.terminalId = id
      this.addBox = {
        show: true,
        title: '修改',
        submitText: '确定',
        cancleText: '取消',
        width: 1240,
        height: 800
      }
      this.isModify = true
      let prom = new Promise((resolve, reject) => {
        // 请求比对源🌲数据
        this.originTreeData = []
        this.$ajax(this.$api.originTree).then(
          res => {
            if (res.code === 200) {
              // 由于后台传过来的是一个对象结构，所以需要push进一个数组
              this.originTreeData.push(res.content)
              resolve()
            }
          }
        )
      })
      prom.then(() => {
        this._getDetails(id, true)
        this._getDetailsComponent(id, true)
      })
    },
    removeHandle (id) {
      this.delBox = {
        show: true,
        title: '删除',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 280
      }
      this.delId = id
    },
    closeDelBox (val) {
      this.delBox.show = val
    },
    submitDelHandle () {
      let deleteParams = {
        url: this.$api.deleteDevice.url + `/${this.delId}`,
        method: this.$api.deleteDevice.method
      }
      this.$ajax(deleteParams).then(
        res => {
          if (res.code === 200) {
            this.$message({
              type: 'success',
              message: '删除成功!'
            })
            this._getDeviceList()
            this.closeDelBox()
          } else {
            this.$message({
              type: 'warning',
              message: '删除失败!'
            })
          }
        }
      )
    },
    bindComponent (id) {
      this.$router.push({
        name: 'bindcomponent',
        params: {id}
      })
    },
    handleCurrentChange (val) {
      this.deviceReq.current = val
      this._getDeviceList()
    },
    // 比对源勾选回调
    handleCheckChange (data, checked) {
      if (checked) {
        let index0 = this.addFormData.regionIds.indexOf('0')
        if (index0 > -1) this.addFormData.regionIds.splice(index0, 1)
        this.addFormData.regionIds.push(data.id)
      } else {
        let index = this.addFormData.regionIds.indexOf(data.id)
        if (index > -1) this.addFormData.regionIds.splice(index, 1)
      }
    }
  }
}
