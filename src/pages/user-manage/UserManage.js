import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
import AddModifyManage from './add-modify-manage'
export default {
  data () {
    return {
      userReq: {
        limit: 10, // = size
        offset: 0, // = current
        name: '',
        nameCn: '',
        policeNum: '',
        deptid: ''
      },
      dialogType: '',
      defaultProps: {
        children: [],
        label: 'name',
        isLeaf: 'leaf'
      },
      userData: [],
      detailData: {},
      loading: true,
      breadcrumbList: [
        {name: '用户管理', to: ''},
        {name: '用户列表', to: ''}
      ],
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      show: false,
      // 传给修改的参数
      propsData: {}
    }
  },
  mounted () {
    this._getUserList()
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox,
    AddModifyManage
  },
  methods: {
    // 加载单位部门🌲
    loadNode (node, resolve) {
      if (node.level === 0) {
        let firstReqData = {
          id: localStorage.getItem('departmentId'),
          level: 1
        }
        this.requestTree(resolve, firstReqData.id, firstReqData.level)
      }
      // 其余节点处理
      if (node.level >= 1) {
        // 注意！把resolve传到你自己的异步中去
        this.getIndex(node, resolve)
      }
    },
    // 异步加载叶子节点数据函数
    getIndex (node, resolve) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${node.data.id}&level=${node.data.level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              et.leaf = false
            })
            let data = res.content
            resolve(data)
          }
        }
      )
    },
    // 首次加载一级节点数据函数
    requestTree (resolve, id, level) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${id}&level=${level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              if (level !== 1) {
                et.leaf = true
              } else {
                et.leaf = false
              }
            })
            let data = res.content
            resolve(data)
            this.$nextTick(function () {
              this.$refs.myTree.setCurrentKey(data[0].id)
              this.userReq.deptid = data[0].id
              this._getUserList()
            })
          }
        }
      )
    },
    // 点击树节点的回调
    handleNodeClick (data) {
      this.userReq.deptid = data.id
      this._getUserList()
    },
    _getUserList () {
      this.$ajax(this.$api.userList, this.userReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.userData = res.content
          } else {
            console.error('获取用户列表失败！')
          }
        }
      )
    },
    // 打开对话框组件
    openDialog (type, data) {
      // 对话框根据类型来判断该显示什么内容
      this.dialogType = type
      this.detailData = data
      if (type === '删除') {
        this.box = {
          show: true,
          title: type,
          submitText: '确定',
          cancleText: '取消',
          width: 500,
          height: 280
        }
      }
      if (type === '详细') {
        this.box = {
          show: true,
          title: type,
          submitText: '确定',
          cancleText: null,
          width: 800,
          height: 520
        }
      }
    },
    // 点击对话框的确定按钮的回调
    submitHandle () {
      if (this.dialogType === '删除') {
        let userDeleteParams = {
          url: this.$api.deleteUser.url + this.detailData.id,
          method: this.$api.deleteUser.method
        }
        this.$ajax(userDeleteParams).then(
          res => {
            if (res.code === 200) {
              this.$message({
                type: 'success',
                message: '删除成功!'
              })
              this._getUserList()
              this.closeBox()
            }
          }
        )
      } else {
        this.closeBox()
      }
    },
    // 关闭对话框
    closeBox (val) {
      this.box.show = val
      this.detailData = {}
    },
    handleCurrentChange (val) {
      this.userReq.offset = val
      this._getUserList()
    },
    // 新增回调
    handleAddModify (data) {
      this.show = true
      if (data !== null) {
        this.breadcrumbList[1].name = '修改'
        // 获取详情
        let detailParams = {
          url: this.$api.detailsUser.url + data.id,
          method: this.$api.detailsUser.method
        }
        this.$ajax(detailParams).then(
          res => {
            if (res.code === 200) {
              data.roleIdList = res.content.roleIdList
            }
          }
        )
      } else {
        this.breadcrumbList[1].name = '新增'
      }
      this.propsData = data
    },
    cancelAdd (val) {
      this.breadcrumbList[1].name = '用户列表'
      this.show = val
      this._getUserList()
    }
  }
}
