export default {
  data () {
    return {
      form: {
        name: '',
        nameCn: '',
        policeNum: '',
        phone: '',
        cardNum: '',
        deptId: '',
        isEnable: '1'
      },
      roleList: [],
      selectRoleList: [],
      defaultProps: {
        children: [],
        label: 'name',
        isLeaf: 'leaf'
      },
      rules: {
        name: [
          { required: true, message: '请输入用户名', trigger: 'blur' },
          { pattern: /^[a-zA-Z0-9_-]{4,16}$/, message: '用户名为 4 到 16 位（字母，数字，下划线，减号）', trigger: 'blur' }
        ],
        nameCn: [
          { required: true, message: '请输入姓名', trigger: 'blur' },
          { pattern: /^[a-zA-Z0-9\u4e00-\u9fa5]{1,10}$/, message: '姓名不能包含特殊字符，且长度不能大于10位', trigger: 'blur' }
        ],
        policeNum: [
          { required: true, message: '请输入警号', trigger: 'blur' },
          { pattern: /^\d{6}$/, message: '警号格式有误', trigger: 'blur' }
        ],
        cardNum: [
          { required: true, message: '请输入身份证号码', trigger: 'blur' },
          { pattern: /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)/, message: '身份证号码格式有误', trigger: 'blur' }
        ],
        phone: [
          { pattern: /^1[345789]\d{9}$/, message: '电话号码格式有误', trigger: 'blur' }
        ]
      },
      selectedNode: ''
    }
  },
  props: {
    propsData: {
      type: Object,
      default: {}
    }
  },
  created () {
    this._getRoleSelect()
  },
  mounted () {
    if (this.propsData !== null) {
      this.form = this.propsData
      this.selectedNode = this.propsData.deptName
    }
  },
  methods: {
    // 角色权限
    _getRoleSelect () {
      this.$ajax(this.$api.queryRole).then(
        res => {
          if (res.code === 200) {
            this.roleList = res.content
            setTimeout(() => {
              this.selectRoleList = this.propsData.roleIdList
            }, 300)
          }
        }
      )
    },
    cancelHandle () {
      this.$emit('cancelAdd', false)
    },
    submitHandle (formName) {
      // 验证新增参数是否合法
      this.$refs[formName].validate((valid) => {
        if (valid) {
          if (this.form.deptId === '') {
            this.$message({
              message: '请选择单位部门',
              type: 'warning'
            })
            return false
          }
          if (this.selectRoleList.length === 0) {
            this.$message({
              message: '请选择角色权限',
              type: 'warning'
            })
            return false
          }

          // 参数验证合法后，将参数组成接口需要的格式，调用接口
          let addModifyParams = this.form
          // 等后台添加新增用户权限参数再添加
          addModifyParams.roleIdList = this.selectRoleList
          if (this.propsData !== null) {
            // 发送修改请求 updateUser
            this.$ajax(this.$api.updateUser, addModifyParams).then(
              res => {
                if (res.code === 200) {
                  // 修改成功，关闭添加层，并提示
                  this.cancelHandle()
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                } else {
                  this.$message({
                    message: res.message,
                    type: 'warning'
                  })
                }
              }
            )
          } else {
            // 发送新增请求
            this.$ajax(this.$api.addUser, addModifyParams).then(
              res => {
                if (res.code === 200) {
                  // 添加成功，关闭添加层，并提示
                  this.cancelHandle()
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                } else {
                  this.$message({
                    message: res.message,
                    type: 'warning'
                  })
                }
              }
            )
          }
        } else {
          return false
        }
      })
    },

    // 加载单位部门🌲
    loadNode (node, resolve) {
      if (node.level === 0) {
        let firstReqData = {
          id: localStorage.getItem('departmentId'),
          level: 1
        }
        this.requestTree(resolve, firstReqData.id, firstReqData.level)
      }
      // 其余节点处理
      if (node.level >= 1) {
        // 注意！把resolve传到你自己的异步中去
        this.getIndex(node, resolve)
      }
    },
    // 异步加载叶子节点数据函数
    getIndex (node, resolve) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${node.data.id}&level=${node.data.level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              et.leaf = false
            })
            let data = res.content
            resolve(data)
          }
        }
      )
    },
    // 首次加载一级节点数据函数
    requestTree (resolve, id, level) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${id}&level=${level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              if (level !== 1) {
                et.leaf = true
              } else {
                et.leaf = false
              }
            })
            let data = res.content
            resolve(data)
          }
        }
      )
    },
    // 点击树节点的回调
    handleNodeClick (data) {
      this.form.deptId = data.id
      this.selectedNode = data.name
    },
    _getUserList () {
      this.$ajax(this.$api.userList, this.userReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.userData = res.content
          } else {
            console.error('获取用户列表失败！')
          }
        }
      )
    }
  }
}
