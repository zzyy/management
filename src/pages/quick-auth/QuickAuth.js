/**
 * Created by zzy on 2018/10/22.
 */
import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
export default {
  name: 'quick-auth',
  data () {
    return {
      breadcrumbList: [
        {name: '后台管理', to: ''},
        {name: '快速授权', to: ''}
      ],
      defaultProps: {
        label: 'name',
        isLeaf: 'leaf'
      },
      selectMenuIdList: [],
      menuList: [],
      roleList: [],
      selectRoleList: '',
      roleId: '',
      authParams: {
        menuIds: [],
        roleId: ''
      }
    }
  },
  components: {
    vMenu,
    vBreadcrumb
  },
  mounted () {
    this._getRoleSelect()
    this._getMenuTree()
  },
  methods: {
    // 获取角色list
    _getRoleSelect () {
      this.$ajax(this.$api.queryRole).then(
        res => {
          if (res.code === 200) {
            this.roleList = res.content

            let id = this.$route.query.id
            if (typeof id === 'undefined') {
              this.roleId = res.content[0].id
            } else {
              this.roleId = id
            }
            this.selectCheck(this.roleId)
          }
        }
      )
    },
    // 功能权限
    _getMenuTree () {
      this.$ajax(this.$api.getMenuTree).then(
        res => {
          if (res.code === 200) {
            let level1 = []
            let level2 = []
            let level3 = []
            let data = res.content

            for (let i = 0; i < data.length; i++) {
              if (data[i].type === 0) level1.push(data[i])
              if (data[i].type === 1) level2.push(data[i])
              if (data[i].type === 2) level3.push(data[i])
            }

            level2.forEach(function (l2) {
              l2.children = []
              level3.forEach(function (l3) {
                if (l2.menuId === l3.parentId) l2.children.push(l3)
              })
            })

            level1.forEach(function (l1) {
              l1.children = []
              level2.forEach(function (l2) {
                if (l1.menuId === l2.parentId) l1.children.push(l2)
              })
            })

            this.menuList = level1
          }
        }
      )
    },
    selectCheck (id) {
      this.roleId = id
      this._getRoleListHandle(id)
    },
    _getRoleListHandle (id) {
      // 获取角色列表
      let detailParams = {
        url: this.$api.detailRole.url + id,
        method: this.$api.detailRole.method
      }
      this.$ajax(detailParams).then(
        res => {
          if (res.code === 200) {
            this.$refs.tree.setCheckedKeys([])
            this.selectMenuIdList = res.content.menuIdList
            this.$nextTick(function () {
              this.selectMenuIdList.forEach(id => {
                this.$refs.tree.setChecked(id, true)
              })
            })
          }
        }
      )
    },
    // 提交授权
    submitHandle () {
      // let checkTree = this.$refs.tree.getCheckedKeys().concat(this.$refs.tree.getHalfCheckedKeys())
      // console.log(checkTree)
      this.authParams.menuIds = this.$refs.tree.getCheckedKeys()
      this.authParams.roleId = this.roleId
      this.$ajax(this.$api.quickAuth, this.authParams).then(
        res => {
          if (res.code === 200) {
            this.$message({
              message: '授权成功',
              type: 'success'
            })
            this._getRoleListHandle(this.roleId)
          }
        }
      )
    },
    // 重置
    resetHandle () {
      this._getRoleListHandle(this.roleId)
    }
  }
}
