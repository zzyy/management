import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
export default {
  data () {
    return {
      rangeReq: {
        limit: 12,
        offset: 1,
        departmentId: '',
        inventedDepartmentName: ''
      },
      addFormData: {
        inventedDepartmentName: '',
        departmentId: '',
        id: ''
      },
      treeData: [],
      defaultProps: {
        children: [],
        label: 'name',
        isLeaf: 'leaf'
      },
      rangeData: [],
      loading: true,
      breadcrumbList: [
        {name: '后台管理', to: ''},
        {name: '范围管理', to: ''}
      ],
      addBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delId: '',
      queryRules: {
        inventedDepartmentName: [
          {min: 1, max: 50, message: '长度在1到50个字符', trigger: 'blur'},
          {pattern: /^[a-z\d\u4e00-\u9fa5]+$/i, message: '范围名称不能包含特殊字符'}
        ]
      },
      addRules: {
        inventedDepartmentName: [
          {required: true, message: '请输入范围名称', trigger: 'blur'},
          {min: 1, max: 20, message: '长度在1到20个字符', trigger: 'blur'},
          {pattern: /^[a-z\d\u4e00-\u9fa5]+$/i, message: '范围名称不能包含特殊字符'}
        ]
      }
    }
  },
  mounted () {

  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox
  },
  methods: {
    // 加载单位部门🌲
    loadNode (node, resolve) {
      if (node.level === 0) {
        let firstReqData = {
          id: localStorage.getItem('departmentId'),
          level: 1
        }
        this.requestTree(resolve, firstReqData.id, firstReqData.level)
      }
      // 其余节点处理
      if (node.level >= 1) {
        // 注意！把resolve传到你自己的异步中去
        this.getIndex(node, resolve)
      }
    },
    // 异步加载叶子节点数据函数
    getIndex (node, resolve) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${node.data.id}&level=${node.data.level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              et.leaf = false
            })
            let data = res.content
            resolve(data)
          }
        }
      )
    },
    // 首次加载一级节点数据函数
    requestTree (resolve, id, level) {
      let departmentParams = {
        url: `${this.$api.selectDepartment.url}?id=${id}&level=${level}`,
        method: this.$api.selectDepartment.method
      }
      this.$ajax(departmentParams).then(
        res => {
          if (res.code === 200) {
            // 处理节点是否是叶子节点
            res.content.forEach(et => {
              if (level !== 1) {
                et.leaf = true
              } else {
                et.leaf = false
              }
            })
            let data = res.content
            resolve(data)
            this.$nextTick(function () {
              this.$refs.myTree.setCurrentKey(data[0].id)
              this.rangeReq.departmentId = data[0].id
              this.addFormData.departmentId = data[0].id
              this._getRangeList()
            })
          }
        }
      )
    },
    // 单击子树查询范围
    handleNodeClick (data) {
      this.rangeReq.departmentId = data.id
      this.addFormData.departmentId = data.id
      this._getRangeList()
    },
    // 查询范围列表
    _getRangeList () {
      this.$ajax(this.$api.rangeList, this.rangeReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.rangeData = res.content
          } else {
            console.error('获取范围列表失败！')
          }
        }
      )
    },
    // 点击分页
    handleCurrentChange (val) {
      this.rangeReq.offset = val
      this._getRangeList()
    },
    // 打开新增弹出层
    addHandle () {
      this.addBox = {
        show: true,
        title: '新增',
        submitText: '确定',
        cancleText: '取消',
        width: 800,
        height: 360
      }
      this.addFormData.id = ''
    },
    // 提交新增请求
    submitAddHandle () {
      this.$refs.addForm.validate((valid) => {
        if (valid) {
          this.$ajax(this.$api.updateRange, this.addFormData).then(
            res => {
              if (res.code === 200) {
                this.$message({
                  type: 'success',
                  message: res.message
                })
                this._getRangeList()
                this.closeAddBox(false)
              } else if (res.code === 600) {
                this.$message({
                  type: 'warning',
                  message: res.message
                })
              }
            }
          )
        }
      })
    },
    closeAddBox (val) {
      this.addBox.show = val
      this.$refs.addForm.resetFields()
      this.addFormData.inventedDepartmentName = ''
    },
    // 修改
    modifyHandle (data) {
      this.addBox = {
        show: true,
        title: '修改',
        submitText: '确定',
        cancleText: '取消',
        width: 800,
        height: 360
      }
      this.addFormData.id = data.id
      this.addFormData.inventedDepartmentName = data.inventedDepartmentName
    },
    removeHandle (id) {
      this.delBox = {
        show: true,
        title: '删除',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 280
      }
      this.delId = id
    },
    closeDelBox (val) {
      this.delBox.show = val
    },
    submitDelHandle () {
      let deleteParams = {
        url: this.$api.deleteRange.url + `${this.delId}`,
        method: this.$api.deleteRange.method
      }
      this.$ajax(deleteParams).then(
        res => {
          if (res.code === 200) {
            this.$message({
              type: 'success',
              message: '删除成功!'
            })
            this._getRangeList()
            this.closeDelBox()
          } else {
            this.$message({
              type: 'warning',
              message: '删除失败!'
            })
          }
        }
      )
    }
  }
}
