/**
 * Created by zzy on 2018/8/10.
 */
import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
import vCollapse from 'components/collapse'
export default {
  data () {
    return {
      breadcrumbList: [
        {name: '后台管理', to: ''},
        {name: '比对源管理', to: ''}
      ],
      originForm: {
        size: 12,
        current: 1,
        regionRegioncode: '', // 编码
        regionName: '', // 比对源名称
        regionAlarmlevel: '', // 报警级别
        parentName: '', // 上级名称
        regionParentid: '' // 父级id
      },
      loading: true,
      treeNodeKey: 'myTree',
      treeData: [],
      defaultProps: {
        children: 'region',
        label: 'name'
      },
      originTableData: [],
      radio: '0',
      validity: '',
      showAddBtn: true,
      checkTreeId: '',
      btnClickFlag: false,
      addTypeFlag: '',
      isModify: false,
      addBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delBox: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      delId: '',
      addFormData: {
        id: '',
        regionParentid: '', // 父id
        regionName: '', // 比对源名称
        regionAlarmlevel: '', // 报警级别
        regionPhotomatchflag: '', // 是否启用图像对比
        regionExpirationdatetime: '' // 有效期截止日期
      },
      rules: {
        regionName: [
          {required: true, message: '请输入比对源名称', trigger: 'blur'},
          {min: 1, max: 10, message: '长度在1到10个字符', trigger: 'blur'},
          {pattern: /^[a-z\d\u4e00-\u9fa5]+$/i, message: '比对源名称不能包含特殊字符'}
        ]
      }
    }
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox,
    vCollapse
  },
  mounted () {
    this._getTreeData()
  },
  watch: {
    radio (val) {
      if (val === '1' && this.validity.length < 8) {
        this.validity = ''
      }
    }
  },
  methods: {
    _getTreeData () {
      this.$ajax(this.$api.originTree).then(
        res => {
          if (res.code === 200) {
            this.treeData = []
            // 由于后台传过来的是一个对象结构，所以需要push进一个数组
            this.treeData.push(res.content)
            // 树的第一条默认选中
            this.$nextTick(function () {
              if (this.checkTreeId === '') {
                this.$refs.myTree.setCurrentKey(res.content.id)
              } else {
                this.$refs.myTree.setCurrentKey(this.checkTreeId)
              }
            })
            this.addFormData.regionParentid = res.content.id
            this._getOriginList()

            // 只有在addType为1的情况下才显示添加按钮
            if (this.addTypeFlag === '') {
              this.addTypeFlag = res.content.addType

              if (res.content.addType === '1') {
                this.showAddBtn = true
                this.btnClickFlag = true // 可以修改删除
              } else {
                this.showAddBtn = false
                this.btnClickFlag = false // 不可以修改删除
              }
            }
          }
        }
      )
    },
    _getOriginList () {
      this.$ajax(this.$api.regionList, this.originForm).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.originTableData = res.content
          }
        }
      )
    },
    removeHandle (id) {
      this.delBox = {
        show: true,
        title: '删除',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 280
      }
      this.delId = id
    },
    closeDelBox (val) {
      this.delBox.show = val
    },
    submitDelHandle () {
      let deleteParams = {
        url: this.$api.deleteOrigin.url + `/${this.delId}`,
        method: this.$api.deleteOrigin.method
      }
      this.$ajax(deleteParams).then(
        res => {
          if (res.code === 200) {
            this._getTreeData()
            this.$message({
              type: 'success',
              message: '删除成功!'
            })
            this._getOriginList()
            this.closeDelBox()
          } else {
            this.$message({
              type: 'warning',
              message: '删除失败!'
            })
          }
        }
      )
    },
    // 打开修改弹出层
    modifyHandle (data) {
      this.addBox = {
        show: true,
        title: '修改',
        submitText: '确定',
        cancleText: '取消',
        width: 900,
        height: 500
      }
      this.isModify = true
      if (data.regionExpirationdatetime === '长期有效') {
        this.radio = '0'
        this.validity = '0'
      } else {
        this.radio = '1'
        this.validity = data.regionExpirationdatetime
      }
      this.addFormData = {
        id: data.id,
        regionParentid: this.$refs.myTree.getCurrentKey(), // 父id
        regionName: data.regionName, // 比对源名称
        regionAlarmlevel: data.alarmlevel, // 报警级别
        regionPhotomatchflag: data.photomatchflag, // 是否启用图像对比
        regionExpirationdatetime: this.validity // 有效期截止日期
      }
    },
    // 打开新增弹出层
    addHandle () {
      this.addBox = {
        show: true,
        title: '新增',
        submitText: '确定',
        cancleText: '取消',
        width: 900,
        height: 500
      }
      this.isModify = false
    },
    // 提交新增请求
    submitAddHandle () {
      this.$refs.addForm.validate((valid) => {
        if (valid) {
          if (this.radio === '0') {
            this.addFormData.regionExpirationdatetime = '0'
          } else {
            if (this.validity === '') {
              this.$message({type: 'warning', message: '请选择有效期'})
              return false
            } else {
              this.addFormData.regionExpirationdatetime = this.validity
            }
          }
          this.tempAddData = {
            id: '',
            regionParentid: this.addFormData.regionParentid, // 父id
            regionName: this.addFormData.regionName, // 比对源名称
            regionAlarmlevel: this.addFormData.regionAlarmlevel, // 报警级别
            regionPhotomatchflag: this.addFormData.regionPhotomatchflag, // 是否启用图像对比
            regionExpirationdatetime: this.addFormData.regionExpirationdatetime // 有效期截止日期
          }
          // 新增
          this.request = this.$api.addOrigin
          this.msg = '新增'
          if (this.isModify) {
            this.request = this.$api.updateOrigin
            this.msg = '修改'

            this.tempAddData.id = this.addFormData.id
            switch (this.addFormData.regionAlarmlevel) {
              case '正常':
                this.tempAddData.regionAlarmlevel = '0'
                break
              case '严重报警':
                this.tempAddData.regionAlarmlevel = '1'
                break
              case '重要报警':
                this.tempAddData.regionAlarmlevel = '2'
                break
              case '一般报警':
                this.tempAddData.regionAlarmlevel = '3'
                break
            }
            switch (this.addFormData.regionPhotomatchflag) {
              case '停用':
                this.tempAddData.regionPhotomatchflag = '0'
                break
              case '启用':
                this.tempAddData.regionPhotomatchflag = '1'
                break
            }
          }
          console.log(this.tempAddData)
          this.$ajax(this.request, this.tempAddData).then(
            res => {
              if (res.code === 200) {
                this.$message({type: 'success', message: `${this.msg}成功!`})
                this._getOriginList()
                this._getTreeData()
                this.closeAddBox(false)
              } else {
                this.$message({type: 'warning', message: `${this.msg}失败!`})
              }
            }
          )
        }
      })
    },
    closeAddBox (val) {
      this.addBox.show = val

      this.$refs.addForm.resetFields()

      this.radio = '0'
      this.validity = ''
      this.addFormData = {
        regionParentid: this.$refs.myTree.getCurrentKey(), // 父id
        regionName: '', // 比对源名称
        regionAlarmlevel: '', // 报警级别
        regionPhotomatchflag: '', // 是否启用图像对比
        regionExpirationdatetime: '' // 有效期截止日期
      }
    },
    handleNodeClick (data) {
      this.originForm.current = 1
      this.originForm.regionParentid = data.id
      this.addFormData.regionParentid = data.id
      this.checkTreeId = data.id
      this._getOriginList()

      // 只有在addType为1的情况下才显示添加按钮
      this.addTypeFlag = data.addType
      if (data.addType === '1') {
        this.showAddBtn = true
        this.btnClickFlag = true // 可以修改删除
      } else {
        this.showAddBtn = false
        this.btnClickFlag = false // 不可以修改删除
      }
    },
    handleCurrentChange (val) {
      this.originForm.current = val
      this._getOriginList()
    }
  }
}
