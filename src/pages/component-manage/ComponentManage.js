import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
export default {
  data () {
    return {
      componentReq: {
        size: 10,
        current: 1,
        fittingsName: '',
        fittingsType: '',
        fittingsModel: '',
        fittingsManufacturer: ''
      },
      sendParams: {
        fittingsName: '',
        fittingsType: '',
        fittingsModel: '',
        fittingsManufacturer: ''
      },
      isModify: true,
      componentTypes: [],
      componentData: [],
      loading: true,
      breadcrumbList: [
        {name: '部件管理', to: ''},
        {name: '部件列表', to: ''}
      ],
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      rules: {
        fittingsName: [
          {required: true, message: '请输入部件名称', trigger: 'blur'},
          {min: 1, max: 50, message: '长度在1到50个字符', trigger: 'blur'},
          {pattern: /^[a-z\d\u4e00-\u9fa5]+$/i, message: '部件名称不能包含特殊字符'}
        ],
        fittingsType: [
          {required: true, message: '请选择部件类型', trigger: 'change'}
        ],
        fittingsModel: [
          {required: true, message: '请输入型号', trigger: 'blur'}
        ],
        fittingsManufacturer: [
          {required: true, message: '请输入厂商', trigger: 'blur'}
        ]
      }
    }
  },
  mounted () {
    this._getComponentType()
    this._getComponentList()
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox
  },
  methods: {
    _getComponentType () {
      this.$ajax(this.$api.componentTypeList).then(
        res => {
          if (res.code === 200) {
            this.componentTypes = res.content
          } else {
            console.error('获取部件类型失败！')
          }
        }
      )
    },
    _getComponentList () {
      this.$ajax(this.$api.componentList, this.componentReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.componentData = res.content
          } else {
            console.error('获取部件列表失败！')
          }
        }
      )
    },
    addClick () {
      this.isModify = false
      this.box = {
        show: true,
        title: '新增',
        submitText: '新增',
        cancleText: '取消',
        width: 600,
        height: 490
      }
      // clear input
      this.sendParams = {
        fittingsName: '',
        fittingsType: '',
        fittingsModel: '',
        fittingsManufacturer: ''
      }
    },
    modifyClick (data) {
      this.isModify = true
      this.box = {
        show: true,
        title: '修改',
        submitText: '修改',
        cancleText: '取消',
        width: 600,
        height: 490
      }
      // modify need id
      this.sendParams = {
        id: data.id,
        fittingsName: data.fittingsName,
        fittingsType: data.fittingsType,
        fittingsModel: data.fittingsModel,
        fittingsManufacturer: data.fittingsManufacturer
      }
    },
    submitHandle () {
      this.$refs.addForm.validate((valid) => {
        if (valid) {
          if (!this.isModify) {
            // add
            this.$ajax(this.$api.addComponent, this.sendParams).then(
              res => {
                if (res.code === 200) {
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                  this._getComponentList()
                  this.closeBox()
                } else {
                  console.error('新增失败！')
                }
              }
            )
          } else {
            this.$ajax(this.$api.modifyComponent, this.sendParams).then(
              res => {
                if (res.code === 200) {
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                  this._getComponentList()
                  this.closeBox()
                } else {
                  console.error('修改失败！')
                }
              }
            )
          }
        } else {
          return false
        }
      })
    },
    closeBox (val) {
      this.box.show = val
    },
    handleCurrentChange (val) {
      this.componentReq.current = val
      this._getComponentList()
    }
  }
}
