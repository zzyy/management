export default {
  data () {
    return {
      form: {
        isEnable: '',
        memo: '',
        menuIdList: [],
        name: '',
        nameCn: ''
      },
      menuList: [],
      defaultProps: {
        label: 'name',
        isLeaf: 'leaf'
      },
      rules: {
        name: [
          { required: true, message: '请输入角色编码', trigger: 'blur' },
          { pattern: /^[a-zA-Z0-9]{4,16}$/, message: '角色编码为 4 到 16 位字母，数字', trigger: 'blur' }
        ],
        nameCn: [
          { required: true, message: '请输入角色名称', trigger: 'blur' },
          { pattern: /^[a-zA-Z0-9\u4e00-\u9fa5]{1,10}$/, message: '角色名称不能包含特殊字符，且长度不能大于10位', trigger: 'blur' }
        ],
        isEnable: [
          { required: true, message: '请选择使用状态', trigger: 'change' }
        ],
        memo: [
          { pattern: /^.{0,50}$/, message: '角色描述不能超过50个字符，且不可换行', trigger: 'blur' }
        ]
      }
    }
  },
  props: {
    propsData: {
      type: Object,
      default: {}
    }
  },
  mounted () {
    if (this.propsData !== null) {
      this.form = this.propsData
    }
    this._getMenuTree()
  },
  methods: {
    _getMenuTree () {
      this.$ajax(this.$api.getMenuTree).then(
        res => {
          if (res.code === 200) {
            let level1 = []
            let level2 = []
            let level3 = []
            let data = res.content

            for (let i = 0; i < data.length; i++) {
              if (data[i].type === 0) level1.push(data[i])
              if (data[i].type === 1) level2.push(data[i])
              if (data[i].type === 2) level3.push(data[i])
            }

            level2.forEach(function (l2) {
              l2.children = []
              level3.forEach(function (l3) {
                if (l2.menuId === l3.parentId) l2.children.push(l3)
              })
            })

            level1.forEach(function (l1) {
              l1.children = []
              level2.forEach(function (l2) {
                if (l1.menuId === l2.parentId) l1.children.push(l2)
              })
            })

            this.menuList = level1
          }
        }
      )
    },
    cancelHandle () {
      this.$emit('cancelAdd', false)
    },
    submitHandle (formName) {
      this.form.menuIdList = this.$refs.tree.getCheckedKeys()
      // 验证新增参数是否合法
      this.$refs[formName].validate((valid) => {
        if (valid) {
          if (!this.form.menuIdList.length) {
            this.$message({
              message: '请选择功能权限',
              type: 'warning'
            })
            return false
          }

          // 参数验证合法后，将参数组成接口需要的格式，调用接口
          let addModifyParams = this.form
          if (this.propsData !== null) {
            // 发送修改请求 updateUser
            this.$ajax(this.$api.updateRole, addModifyParams).then(
              res => {
                if (res.code === 200) {
                  // 修改成功，关闭添加层，并提示
                  this.cancelHandle()
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                } else {
                  this.$message({
                    message: res.message,
                    type: 'warning'
                  })
                }
              }
            )
          } else {
            // 发送新增请求
            this.$ajax(this.$api.addRole, addModifyParams).then(
              res => {
                if (res.code === 200) {
                  // 添加成功，关闭添加层，并提示
                  this.cancelHandle()
                  this.$message({
                    message: res.message,
                    type: 'success'
                  })
                } else {
                  this.$message({
                    message: res.message,
                    type: 'warning'
                  })
                }
              }
            )
          }
        } else {
          return false
        }
      })
    }
  }
}
