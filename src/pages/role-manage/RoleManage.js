import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
import AddModifyManage from './add-modify-manage'
export default {
  data () {
    return {
      roleReq: {
        limit: 10, // = size
        offset: 1, // = current
        roleName: '',
        isEnable: ''
      },
      roleData: [],
      loading: true,
      roleId: '',
      breadcrumbList: [
        {name: '角色管理', to: ''},
        {name: '角色列表', to: ''}
      ],
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      },
      show: false,
      // 传给修改的参数
      propsData: {}
    }
  },
  mounted () {
    this._getRoleList()
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox,
    AddModifyManage
  },
  methods: {
    _getRoleList () {
      this.$ajax(this.$api.getRoleList, this.roleReq).then(
        res => {
          if (res.code === 200) {
            this.loading = false
            this.roleData = res.content
          } else {
            console.error('获取角色列表失败！')
          }
        }
      )
    },
    // 删除对话框
    handleDelete (data) {
      this.roleId = data.id
      this.box = {
        show: true,
        title: '删除',
        submitText: '确定',
        cancleText: '取消',
        width: 500,
        height: 280
      }
    },
    // 点击对话框的确定按钮的回调
    deleteSubmitHandle () {
      let roleDeleteParams = {
        url: this.$api.deleteRole.url + this.roleId,
        method: this.$api.deleteRole.method
      }
      this.$ajax(roleDeleteParams).then(
        res => {
          if (res.code === 200) {
            this.$message({
              type: 'success',
              message: res.message
            })
            this._getRoleList()
            this.closeBox()
          }
        }
      )
    },
    // 关闭对话框
    closeBox (val) {
      this.box.show = val
    },
    handleCurrentChange (val) {
      this.roleReq.offset = val
      this._getRoleList()
    },
    // 新增回调
    handleAddModify (data) {
      this.show = true
      if (data !== null) {
        this.breadcrumbList[1].name = '修改'
        // 获取详情
        let detailParams = {
          url: this.$api.detailRole.url + data.id,
          method: this.$api.detailRole.method
        }
        this.$ajax(detailParams).then(
          res => {
            if (res.code === 200) {
              data.menuIdList = res.content.menuIdList
            }
          }
        )
      } else {
        this.breadcrumbList[1].name = '新增'
      }
      this.propsData = data
    },
    cancelAdd (val) {
      this.breadcrumbList[1].name = '角色列表'
      this.show = val
      this._getRoleList()
    },
    // 去到快速授权页
    toQuickAuth (id) {
      this.$router.push({
        path: `quick?id=${id}`
      })
    }
  }
}
