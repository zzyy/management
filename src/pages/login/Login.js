import {getMenuFuc} from '../../router/index'
export default {
  data () {
    return {
      form: {
        username: '',
        password: ''
      }
    }
  },
  methods: {
    handleLogin () {
      this.$ajax(this.$api.login, this.form).then(
        res => {
          if (res.code === 200) {
            localStorage.setItem('token', res.content.token)
            localStorage.setItem('username', res.content.userNameCn)
            localStorage.setItem('departmentId', res.content.deptId)

            this.getMenuAuth()
          } else {
            this.$message({
              message: res.message,
              type: 'warning'
            })
          }
        }
      )
    },
    getMenuAuth () {
      this.$ajax(this.$api.menuAuth).then(
        res => {
          if (res.code === 200) {
            let menu = res.content.menuList
            if (menu.length === 0) {
              this.$message({
                message: '该用户没有添加菜单权限，无法登录系统',
                type: 'warning'
              })
              return false
            }
            localStorage.setItem('menuTree', JSON.stringify(menu))
            localStorage.setItem('permissions', JSON.stringify(res.content.permissions))
            getMenuFuc(menu)
            if (typeof menu[0] !== 'undefined') {
              if (typeof menu[0].list[0] !== 'undefined') {
                this.$router.push({name: menu[0].list[0].url})
              }
            }
          } else {
            this.$message({
              message: res.message,
              type: 'warning'
            })
          }
        }
      )
    }
  }
}
