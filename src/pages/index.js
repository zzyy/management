import ComponentManage from './component-manage'
import {DeviceManage, BindComponent} from './device-manage'
import StateMonitor from './state-monitor'
import UserManage from './user-manage'
import RoleManage from './role-manage'
import Login from './login'
import OriginManage from './origin-manage'
import RangeManage from './range-manage'
import QuickAuth from './quick-auth'
import Err404 from './err404'
export {
  ComponentManage,
  DeviceManage,
  BindComponent,
  StateMonitor,
  UserManage,
  RoleManage,
  Login,
  OriginManage,
  RangeManage,
  QuickAuth,
  Err404
}
