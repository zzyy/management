import vMenu from 'components/menu'
import vBreadcrumb from 'components/breadcrumb'
import vMessageBox from 'components/message-box'
import vCollapse from 'components/collapse'
export default {
  data () {
    return {
      breadcrumbList: [
        {name: '状态监控', to: ''},
        {name: '设备列表', to: ''}
      ],
      filterText: '',
      treeNodeKey: 'myTree',
      treeData: [],
      defaultProps: {
        children: 'deptTree',
        label: 'name'
      },
      deviceTypesAddOrMod: [],
      devicesData: [],
      detailsData: {},
      detailsOfComponent: [],
      box: {
        show: false,
        title: '',
        submitText: '',
        cancleText: '',
        width: 0,
        height: 0
      }
    }
  },
  components: {
    vMenu,
    vBreadcrumb,
    vMessageBox,
    vCollapse
  },
  mounted () {
    this._getTreeData()
  },
  watch: {
    filterText (val) {
      this.$refs.myTree.filter(val)
    }
  },
  methods: {
    _getTreeData () {
      this.$ajax(this.$api.departmentTree).then(
        res => {
          if (res.code === 200) {
            // 由于后台传过来的是一个对象结构，所以需要push进一个数组
            this.treeData = res.content
            this.$nextTick(function () {
              this.$refs.myTree.setCurrentKey(res.content[0].id)
            })
            this._getOrgDeviceOfArea(res.content[0].id)
          }
        }
      )
      // 获取设备类型下拉 新增 或 修改
      this.$ajax(this.$api.deviceTypeListAddOrMod).then(
        res => {
          if (res.code === 200) {
            this.deviceTypesAddOrMod = res.content
          }
        }
      )
    },
    _getOrgDeviceOfArea (id) {
      let orgDeviceData = {
        url: this.$api.orgDeviceOfArea.url + id,
        method: this.$api.orgDeviceOfArea.method
      }
      this.$ajax(orgDeviceData).then(
        res => {
          if (res.code === 200) {
            this.devicesData = res.content
          }
        }
      )
    },
    _getDetails (id) {
      // left details
      let deviceDetail = {
        url: this.$api.deviceDetail.url + id,
        method: this.$api.deviceDetail.method
      }
      this.$ajax(deviceDetail).then(
        res => {
          if (res.code === 200) {
            let terminalType = res.content.faceTerminalType
            this.deviceTypesAddOrMod.forEach(item => {
              if (item.typeCode === terminalType) {
                res.content.showTerminalType = item.typeName
              }
            })
            this.detailsData = res.content
          }
        }
      )
    },
    _getDetailsComponent (id) {
      // right details
      let componentOfDeviceForGroup = {
        url: this.$api.componentOfDeviceForGroup.url + id,
        method: this.$api.componentOfDeviceForGroup.method
      }
      this.$ajax(componentOfDeviceForGroup).then(
        res => {
          if (res.code === 200) {
            this.detailsOfComponent = res.content
          }
        }
      )
    },
    handleNodeClick (data) {
      this._getOrgDeviceOfArea(data.id)
    },
    filterNode (value, data) {
      if (!value) return true
      return data.name.indexOf(value) !== -1
    },
    handleDetails (id, status) {
      // if (status === '离线') {
      //   this.$message({
      //     message: '设备离线不能查看详情',
      //     type: 'warning'
      //   })
      //   return false
      // }
      this.box = {
        show: true,
        title: '查看详情',
        submitText: '确定',
        cancleText: null,
        width: 1360,
        height: 650
      }
      // clear detailsData
      this.detailsData = {}
      this._getDetails(id)
      this._getDetailsComponent(id)
    },
    submitHandle () {
      this.closeBox()
    },
    closeBox (val) {
      this.box.show = val
    }
  }
}
